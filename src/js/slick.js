export class SlickSlider {
  constructor(obj) {
    const $slider = $(`.${obj.elementClassName}`);
    const slickConfig = obj.slickConfig ? obj.slickConfig : null;

    if ($slider && $slider.length) {
      $slider.slick(slickConfig);
    }
  }
}
