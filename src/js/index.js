import {
  SlickSlider
} from './slick';

let mainSlider = new SlickSlider({
  elementClassName: 'main_slider',
  slickConfig: {
    slidesToShow: 1,
    dots: true,
  }
});

class OtherSlider extends SlickSlider {
  constructor() {
    super({
      elementClassName: 'other_slider',
      slickConfig: {
        slidesToShow: 1,
        dots: false,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 2000,
      }
    });
  }

  ownMethod() {
    console.log('do anything there');
  }
}

const otherSlider = new OtherSlider();
otherSlider.ownMethod();
