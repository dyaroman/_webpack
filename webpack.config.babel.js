import webpack from 'webpack';
import path from 'path';
import autoprefixer from 'autoprefixer';
import ExtractTextPlugin from 'extract-text-webpack-plugin';

const extractStyles = new ExtractTextPlugin({
  filename: 'build.css'
});

const config = {
  context: path.resolve(__dirname, './src'),
  devtool: 'source-map',
  entry: {
    app: './app.js',
  },
  output: {
    path: path.resolve(__dirname, './dest'),
    filename: 'build.js',
  },
  module: {
    rules: [{
        test: /\.scss$/,
        use: extractStyles.extract({
          use: [
            {
              loader: 'css-loader',
              options: {sourceMap: 'true'}
            },
            {
              loader: 'postcss-loader',
              options: {sourceMap: 'true'}
            },
            {
              loader: 'sass-loader',
              options: {sourceMap: 'true'}
            }
          ]
        })
      },
      {
        test: /\.js$/,
        exclude: [/node_modules/],
        use: [{
          loader: 'babel-loader',
          options: { presets: ['es2015'] },
        }],
      }
    ]
  },
  plugins: [
    new webpack.LoaderOptionsPlugin({
      minimize: false,
      debug: true,
      options: {
        postcss: [
          autoprefixer({
            browsers: ['last 2 version']
          })
        ],
        sassLoader: {
          includePaths: [
            // path.resolve(__dirname, 'node_modules/sanitize.css/')
          ]
        }
      }
    }),
    extractStyles,
  ]
};

export default config;
